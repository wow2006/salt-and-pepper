<!--
.. title: Use modern CPP with CMake
.. slug: use-modern-cpp-with-cmake
.. date: 2018-10-27 17:03:16 UTC+02:00
.. tags: cmake, cpp
.. category: 
.. link: 
.. description: 
.. type: text
-->

I want to share my experance with using CPP and CMake.
I will write simple tip every time. 

#### Problem:
I want to use `c++14` with cmake.

#### Solution:
I use multiable ways to do that. Every way, I will tell every way I know and finally which way I use it.


##### 1. Simplest:
`CMake` contains `add_definintions` command which add flags globally.

> hm, It look good and we can hack every thing with it.

I want to tell GCC compiler to use c++14. For simple compile command I just add `-std=c++14` to compile command.
```bash
> gcc -std=c++14 main.cpp -o main
```
I will call `add_definitions` with `-std=c++14`. It will work yaaaaaaay.

> Are you sure?

```bash
> # TODO
> git clone url
> cd $src
> mkdir build && cd build
> cmake ..
> cmake --build .
```

Now every thing is working and everyone is happy.

> STOP THERE
> That will work for GNU or clang only.

That is true. I should check for operting system.

```cmake
if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU" OR
   CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
    add_definitions(-std=c++14)
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
    add_definitions(/std:c++14)
endif()
```
Done with this post salut.
> not quite finished, What if GNU version don't support c++14. Compiler will complian.

```
g++: error: unrecognized command line option '-std=c++14'
```
> what say you?!
